<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Repositories\PostRepository;
use App\Http\Requests\Post\CreateRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Http\Resources\Post\PostCollection;
use App\Http\Resources\Post\PostDetail;
use App\Http\Services\Searches\PostSearch;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index(Request $request)
    {
        $factory = app()->make(PostSearch::class);
        $result = $factory->apply()->paginate($request->per_page);

        return new PostCollection($result);
    }

    public function store(CreateRequest $request)
    {
        $result = $this->postRepository->store($request->all());

        return $result;
    }

    public function show($slug)
    {
        $result = $this->postRepository->findByCriteria($slug);

        return new PostDetail($result);
    }

    public function update(UpdateRequest $request, $id)
    {
        $result = $this->postRepository->find($id);
        $result = $this->postRepository->update($request->all(), $result);

        return $result;
    }

    public function delete($id)
    {
        $result = $this->postRepository->find($id);
        $result = $this->postRepository->delete($result);

        return $result;
    }
}
