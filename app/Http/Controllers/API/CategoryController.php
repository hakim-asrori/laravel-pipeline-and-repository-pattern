<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Repositories\CategoryRepository;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryDetail;
use App\Http\Services\Searches\CategorySearch;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
        $factory = app()->make(CategorySearch::class);
        $result = $factory->apply()->paginate($request->per_page);

        return new CategoryCollection($result);
    }

    public function store(CategoryRequest $request)
    {
        $result = $this->categoryRepository->store($request->all());

        return response()->json([
            'message' => 'Create success!',
            'result' => $result
        ]);
    }

    public function show($id)
    {
        $result = $this->categoryRepository->find($id);

        return new CategoryDetail($result);
    }

    public function update(CategoryRequest $request, $id)
    {
        $result = $this->categoryRepository->find($id);

        $this->categoryRepository->update($request->all(), $id);

        return response()->json([
            'message' => 'Update success!',
            'data' => new CategoryDetail($result)
        ]);
    }

    public function delete($id)
    {
        $result = $this->categoryRepository->find($id);

        $this->categoryRepository->delete($id);

        return response()->json([
            'message' => 'Delete success!',
            'data' => new CategoryDetail($result)
        ]);
    }
}
