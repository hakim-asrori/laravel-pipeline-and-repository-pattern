<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Repositories\UserRepository;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserDetail;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $result = $this->userRepository->all();

        return new UserCollection($result);
    }

    public function show($id)
    {
        $result = $this->userRepository->find($id);

        return new UserDetail($result);
    }

    public function update(UpdateRequest $request, $id)
    {
        $result = $this->userRepository->find($id);
        $result = $this->userRepository->update($request->all(), $result);

        return $result;
    }

    public function delete($id)
    {
        $result = $this->userRepository->find($id);
        $result = $this->userRepository->delete($result);

        return $result;
    }

    public function changePassword(ChangePasswordRequest $request, $id)
    {
        $result = $this->userRepository->find($id);
        $result = $this->userRepository->changePassword($request->all(), $result);

        return $result;
    }
}
