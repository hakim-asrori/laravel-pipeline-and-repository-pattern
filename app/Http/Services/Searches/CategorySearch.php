<?php

namespace App\Http\Services\Searches;

use App\Http\Services\Searches\Filters\Category\Name;
use App\Http\Services\Searches\Filters\Category\Sort;
use App\Http\Services\Searches\HttpSearch;
use App\Models\Category;

class CategorySearch extends HttpSearch
{

    protected function passable()
    {
        return Category::query();
    }

    protected function filters(): array
    {
        return [
            Name::class,
            Sort::class
        ];
    }

    protected function thenReturn($categorySearch)
    {
        return $categorySearch;
    }
}
