<?php

namespace App\Http\Services\Searches;

use App\Http\Services\Searches\Filters\Post\Category;
use App\Http\Services\Searches\Filters\Post\CategoryId;
use App\Http\Services\Searches\Filters\Post\Title;
use App\Http\Services\Searches\Filters\Post\Body;
use App\Http\Services\Searches\Filters\Post\Sort;
use App\Http\Services\Searches\HttpSearch;
use App\Models\Post;

class PostSearch extends HttpSearch
{

    protected function passable()
    {
        return Post::query();
    }

    protected function filters(): array
    {
        return [
            Body::class,
            Sort::class,
            Title::class,
            Category::class,
            CategoryId::class
        ];
    }

    protected function thenReturn($postSearch)
    {
        return $postSearch;
    }
}
