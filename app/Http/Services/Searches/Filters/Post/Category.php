<?php

namespace App\Http\Services\Searches\Filters\Post;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Services\Searches\Contracts\FilterContract;

class Category implements FilterContract
{
    /** @var string|null */
    protected $category;

    /**
     * @param string|null $category
     * @return void
     */
    public function __construct($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function handle(Builder $query, Closure $next)
    {
        if (!$this->keyword()) {
            return $next($query);
        }

        $query->when($category = $this->category, function ($q) use ($category) {
            $q->whereHas('category', function ($categoryQuery) use ($category) {
                $categoryQuery->where('name', 'LIKE', '%' . $category . '%');
            });
        });

        return $next($query);
    }

    /**
     * Get category keyword.
     *
     * @return mixed
     */
    protected function keyword()
    {
        if ($this->category) {
            return $this->category;
        }

        $this->category = request('category', null);

        return request('category');
    }
}
