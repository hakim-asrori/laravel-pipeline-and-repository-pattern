<?php

namespace App\Http\Services\Searches\Filters\Post;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Services\Searches\Contracts\FilterContract;

class CategoryId implements FilterContract
{
    /** @var string|null */
    protected $categoryId;

    /**
     * @param string|null $categoryId
     * @return void
     */
    public function __construct($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function handle(Builder $query, Closure $next)
    {
        if (!$this->keyword()) {
            return $next($query);
        }

        $query->where('category_id', 'LIKE', '%' . $this->categoryId . '%');

        return $next($query);
    }

    /**
     * Get categoryId keyword.
     *
     * @return mixed
     */
    protected function keyword()
    {
        if ($this->categoryId) {
            return $this->categoryId;
        }

        $this->categoryId = request('category_id', null);

        return request('category_id');
    }
}
