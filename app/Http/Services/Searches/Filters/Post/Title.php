<?php

namespace App\Http\Services\Searches\Filters\Post;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Services\Searches\Contracts\FilterContract;

class Title implements FilterContract
{
    /** @var string|null */
    protected $title;

    /**
     * @param string|null $title
     * @return void
     */
    public function __construct($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function handle(Builder $query, Closure $next)
    {
        if (!$this->keyword()) {
            return $next($query);
        }
        $query->where('title', 'LIKE', '%' . $this->title . '%');

        return $next($query);
    }

    /**
     * Get title keyword.
     *
     * @return mixed
     */
    protected function keyword()
    {
        if ($this->title) {
            return $this->title;
        }

        $this->title = request('title', null);

        return request('title');
    }
}
