<?php

namespace App\Http\Services\Searches\Filters\Post;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Services\Searches\Contracts\FilterContract;

class Body implements FilterContract
{
    /** @var string|null */
    protected $body;

    /**
     * @param string|null $body
     * @return void
     */
    public function __construct($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function handle(Builder $query, Closure $next)
    {
        if (!$this->keyword()) {
            return $next($query);
        }

        $query->where('body', 'LIKE', '%' . $this->body . '%');

        return $next($query);
    }

    /**
     * Get body keyword.
     *
     * @return mixed
     */
    protected function keyword()
    {
        if ($this->body) {
            return $this->body;
        }

        $this->body = request('body', null);

        return request('body');
    }
}
