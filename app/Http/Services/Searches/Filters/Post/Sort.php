<?php

namespace App\Http\Services\Searches\Filters\Post;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Services\Searches\Contracts\FilterContract;
use App\Models\Post;

class Sort implements FilterContract
{
    protected $defaultSortField = 'id';

    public function classes(): array
    {
        return [
            new Post(),
        ];
    }

    public function handle(Builder $query, Closure $next)
    {
        $orderBy = request('order_by', request()->order_by ? request()->order_by : '');
        $orderDirection = request(
            'order_direction',
            request()->order_direction ?
                request()->order_direction : ''
        );

        $isSortAvailable = $this->isSortFieldAvailable($orderBy);

        if ($isSortAvailable) {
            $query->orderBy($orderBy, $orderDirection);
        }


        return $next($query);
    }

    protected function isSortFieldAvailable(string $sort): bool
    {
        $fillable = $this->getAllFillable();

        return in_array($sort, $fillable);
    }

    protected function getAllFillable(): array
    {
        $classes = $this->classes();
        $fillable = [];

        foreach ($classes as $class) {
            $keys = $class->getTable();

            foreach ($class->getFillable() as $fill) {
                $fillable[] = $keys . '.' . $fill;
            }
        }

        return $fillable;
    }
}
