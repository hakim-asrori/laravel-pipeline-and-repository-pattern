<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contracts\CategoryContract;
use App\Http\Repositories\BaseRepository;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;

class CategoryRepository implements CategoryContract
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function all(): Collection
    {
        return $this->category->all();
    }

    public function store(array $attributes): Category
    {
        $attributes['slug'] = Str::slug($attributes['name']);

        return $this->category->create($attributes);
    }

    public function find($id): Category
    {
        $result = $this->category->find($id);

        if (!$result) {
            return abort(404);
        }

        return $result;
    }

    public function update(array $attributes, $id)
    {
        $result = $this->category->find($id);

        if (!$result) {
            return abort(404);
        }

        $attributes['slug'] = Str::slug($attributes['name']);

        return $result->update($attributes);
    }

    public function delete($id)
    {
        return $this->category->where('id', $id)->delete();
    }
}
