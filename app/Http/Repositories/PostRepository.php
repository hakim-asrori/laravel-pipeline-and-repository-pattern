<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contracts\PostContract;
use App\Http\Repositories\BaseRepository;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PostRepository implements PostContract
{
    /** @var Post */
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function all(): Collection
    {
        return $this->post->all();
    }

    public function store(array $attributes): Collection
    {
        $findCategory = Category::find($attributes['category_id']);
        if (!$findCategory) {
            return collect([
                'message' => 'Fail, Sorry category data not found',
                'status_code' => 400
            ]);
        }

        $attributes['slug'] = Str::slug($attributes['title']);
        $attributes['user_id'] = Auth::user()->id;
        $attributes['excerpt'] = Str::limit($attributes['body'], 100);

        if ($attributes['image']) {
            $attributes['image'] = $attributes['image']->store('public/posts');
            $attributes['image'] = url('') . '/' . str_replace('public', 'storage', $attributes['image']);
        }

        $result = $this->post->create($attributes);

        return collect([
            'message' => 'Create success!',
            'result' => $result
        ]);
    }

    public function find($id): Post
    {
        $result = $this->post->find($id);

        if (!$result) {
            return abort(404);
        }

        return $result;
    }

    public function findByCriteria($slug): Post
    {
        $result = $this->post->where("slug", $slug)->first();

        if (!$result) {
            return abort(404);
        }

        return $result;
    }

    public function update(array $attributes, $result)
    {
        $findCategory = Category::find($attributes['category_id']);
        if (!$findCategory) {
            return collect([
                'message' => 'Fail, Sorry category data not found',
                'status_code' => 400
            ]);
        }

        $attributes['slug'] = Str::slug($attributes['title']);
        $attributes['excerpt'] = Str::limit($attributes['body'], 100);

        if (isset($attributes['image'])) {
            $oldImage = str_replace(url('') . '/storage/', '', $result->image);
            Storage::disk('public')->delete($oldImage);

            $attributes['image'] = $attributes['image']->store('public/posts');
            $attributes['image'] = url('') . '/' . str_replace('public', 'storage', $attributes['image']);
        }

        $result = $result->update($attributes);

        return collect([
            'message' => 'Update success!',
            'result' => $result
        ]);
    }

    public function delete($result)
    {
        $image = str_replace(url('') . '/storage/', '', $result->image);
        Storage::disk('public')->delete($image);

        $result->delete();

        return collect([
            'message' => 'Delete success!',
            'result' => $result
        ]);
    }
}
