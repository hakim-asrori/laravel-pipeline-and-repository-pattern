<?php

namespace App\Http\Repositories;

use App\Http\Repositories\Contracts\UserContract;
use App\Http\Repositories\BaseRepository;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserRepository extends BaseRepository implements UserContract
{
    /** @var User */
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->user = $user;
    }

    public function index(): Collection
    {
        return $this->user->all();
    }

    public function store(array $attributes): User
    {
        $attributes['password'] = Hash::make($attributes['password']);
        if ($attributes['image']) {
            $attributes['image'] = $attributes['image']->store('public/users');
            $attributes['image'] = url('') . '/' . str_replace('public', 'storage', $attributes['image']);
        }

        return $this->user->create($attributes);
    }

    public function update(array $attributes, $result)
    {
        if (isset($attributes['image'])) {
            $oldImage = str_replace(url('') . '/storage/', '', $result->image);
            Storage::disk('public')->delete($oldImage);

            $attributes['image'] = $attributes['image']->store('public/users');
            $attributes['image'] = url('') . '/' . str_replace('public', 'storage', $attributes['image']);
        }

        $result = $result->update($attributes);

        return collect([
            'message' => 'Update success!',
            'result' => $result
        ]);
    }

    public function delete($result)
    {
        if ($result->id == 1) {
            return collect([
                'message' => 'Fail, Sorry user data not found',
                'status_code' => 400
            ]);
        }

        $image = str_replace(url('') . '/storage/', '', $result->image);
        Storage::disk('public')->delete($image);

        $result->delete();

        return collect([
            'message' => 'Delete success!',
            'result' => $result
        ]);
    }

    public function changePassword(array $attributes, $result)
    {
        if (!Hash::check($attributes['old_password'], $result->password)) {
            return collect([
                'message' => 'Fail, Sorry old password wrong',
                'status_code' => 400
            ]);
        }

        if ($attributes['old_password'] == $attributes['new_password']) {
            return collect([
                'message' => 'Fail, Sorry new password same with old password',
                'status_code' => 400
            ]);
        }

        $attributes['password'] = Hash::make($attributes['new_password']);
        unset($attributes['old_password']);
        unset($attributes['new_password']);
        unset($attributes['confirm_password']);

        $result->update($attributes);

        return collect([
            'message' => 'Update success!',
            'result' => $result
        ]);
    }
}
