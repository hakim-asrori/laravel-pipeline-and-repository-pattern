<?php

namespace App\Http\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface UserContract
{
    /**
     * @return mixed
     */
    public function index(): Collection;

    public function store(array $attributes): ?Model;

    public function update(array $attributes, Model $result);

    public function delete(Model $result);

    public function changePassword(array $attributes, Model $result);
}
