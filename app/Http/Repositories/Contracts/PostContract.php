<?php

namespace App\Http\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface PostContract
{
    public function all(): Collection;

    public function store(array $attributes): ?Collection;

    public function find($id): Model;

    public function findByCriteria($slug): Model;

    public function update(array $attributes, Model $result);

    public function delete(Model $result);
}
