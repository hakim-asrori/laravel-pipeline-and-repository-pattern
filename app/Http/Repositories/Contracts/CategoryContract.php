<?php

namespace App\Http\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface CategoryContract
{
    public function all(): Collection;

    public function store(array $attributes): ?Model;

    public function find($id): Model;

    public function update(array $attributes, $id);

    public function delete($id);
}
