<?php

use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\PostController;
use App\Http\Controllers\API\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

includeRouteFiles(__DIR__ . '/api');

Route::prefix('user')->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('api.user.index');
    Route::get('/{id}', [UserController::class, 'show'])->name('api.user.show');
    Route::put('/{id}', [UserController::class, 'update'])->name('api.user.update');
});

Route::prefix('category')->group(function () {
    Route::get('/', [CategoryController::class, 'index'])->name('api.category.index');
    Route::get('/{slug}', [CategoryController::class, 'show'])->name('api.category.show');
});

Route::prefix('post')->group(function () {
    Route::get('/', [PostController::class, 'index'])->name('api.post.index');
    Route::get('/{slug}', [PostController::class, 'show'])->name('api.post.show');
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::prefix('admin')->group(function () {
        // Category
        Route::prefix('category')->group(function () {
            Route::get('/', [CategoryController::class, 'index'])->name('api.admin.category.index');
            Route::post('/', [CategoryController::class, 'store'])->name('api.admin.category.store');
            Route::get('/{id}', [CategoryController::class, 'show'])->name('api.admin.category.show');
            Route::put('/{id}', [CategoryController::class, 'update'])->name('api.admin.category.update');
            Route::delete('/{id}', [CategoryController::class, 'delete'])->name('api.admin.category.delete');
        });

        // Post
        Route::prefix('post')->group(function () {
            Route::get('/', [PostController::class, 'index'])->name('api.admin.post.index');
            Route::post('/', [PostController::class, 'store'])->name('api.admin.post.store');
            Route::get('/{id}', [PostController::class, 'show'])->name('api.admin.post.show');
            Route::put('/{id}', [PostController::class, 'update'])->name('api.admin.post.update');
            Route::delete('/{id}', [PostController::class, 'delete'])->name('api.admin.post.delete');
        });

        Route::delete('user/{id}', [UserController::class, 'delete'])->name('api.admin.user.delete');
    });
});
