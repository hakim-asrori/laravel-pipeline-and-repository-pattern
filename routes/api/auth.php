<?php

use App\Http\Controllers\API\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->group(function () {
    Route::post('login', LoginController::class)->name('api.auth.login');

    Route::post('register', RegisterController::class)->name('api.auth.register');

    Route::middleware(['auth:sanctum'])->group(function () {
        Route::put('change-password/{id}', [UserController::class, 'changePassword'])->name('api.auth.change-password');

        Route::post('check', function () {
            return response()->json([
                'user' => Auth::user()
            ]);
        })->name('api.auth.check');
    });
});
